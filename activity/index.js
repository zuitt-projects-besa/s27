const http = require("http");
const port = 8000;

let items = [
	{
		"name" : "Iphone X",
		"price" : 30000,
		"isActive" : true
	},
	{
		"name" : "Samsung Galaxy S21",
		"price" : 51000,
		"isActive" : true
	},
	{
		"name" : "Razer Blackshark VX2",
		"price" : 2500,
		"isActive" : false
	},
]


const server = http.createServer((req, res) => {

	// GET items[]
	if (req.url === "/items" && req.method === "GET"){

		res.writeHead(200, {'Content-Type' : 'application/json'});
		res.end(JSON.stringify(items));

	// POST items[]
	} else if (req.url === "/items" && req.method === "POST"){
		let requestBody = "";

		req.on('data', (data) => {
			requestBody += data
		});

		req.on('end', () =>{

			requestBody = JSON.parse(requestBody)

			let newItems = {
				name : requestBody.name,
				price : requestBody.price,
				isActive : requestBody.isActive
			}

			items.push(newItems);
			console.log(items);

			res.writeHead(200, {'Content-Type' : 'application/json'});
			res.end(JSON.stringify(items))
		});

	// DELETE items[]
	} else if (req.url === "/items" && req.method === "DELETE"){

		let removedItem = items.pop()
		console.log(items);

		res.writeHead(200, {'Content-Type' : 'application/json'});
		res.end(JSON.stringify(items));
	}
});

server.listen(port);